// ===========================================================================
// This is a quick-and-dirty steering program that procudes Linux shell
// scripts to steer the W Space Exp(Z) fractal computation.
//
// It uses ImageMagick for image manipulation:
/*

Convert from XPM to GIF:
    convert <input file>.xpm <options> <output file>.gif

Options:
    -scale <width>                    Scale picture to <witdh> (keep height ratio)
    -font "<name>"                    selects print font, e.g. "Courier"
    -pointsize <size>                 size of characters in point
    -fill "#<rgb>"                    set draw color
    -draw 'text <x>,<y> "<string>"'   draws <string> at position <x>,<y> from top-left

*/
// You can then e.g. use gifsicle to make an animated GIF, e.g.:
/*

    gifsicle --delay=10 --loop *.gif --colors 256 > anim.gif

*/
// ===========================================================================

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

FILE   *fp;                          // out file pointer

int    runID;
double runIDd;
double zR;
double zW;
double xMin;
double xMax;
double yMin;
double yMax;
int    resX;

// ---------------------------------------------------------------------------
// main
// ---------------------------------------------------------------------------

int main(int argc, char **argv) {
//  here it starts: prepares the log; actual
//  execution is driven in "run-wspaceexpfractal.c"

    fp = fopen("execSteerWExpZ.sh", "wb");
    fprintf(fp, "# command file generated: \n");

    struct tm *local;
    time_t t;

    t = time(NULL);
    local = localtime(&t);
    fprintf(fp, "# %s\n", asctime(local));
    fprintf(fp, "date\n");

    zR = 0.;
    zW = 1.;
    xMin = 0.;
    xMax = 2.;
    yMin = 0.;
    yMax = 2.;
    resX = 1500;
    runID = 100;

    while (runID < 500) {

        runIDd = (((double) runID) - 100.) / 400.;
        runIDd = sqrt( 1 - (runIDd - 1.) * (runIDd - 1.));

        xMin = runIDd * 0.36 + 0. * (1. - runIDd);
        xMax = runIDd * 0.45 + 2. * (1. - runIDd);
        yMin = runIDd * 0.01 + 0. * (1. - runIDd);
        yMax = runIDd * 0.10 + 2. * (1. - runIDd);

        fprintf(fp,
                "./calcWExpZ %d %le %le %le %le %le %le %d\n",
                runID,
                zR,
                zW,
                xMin,
                xMax,
                yMin,
                yMax,
                resX);

        fprintf(fp, "convert pic-wspaceexpfractal-%04d.xpm ", runID);
        fprintf(fp, "-scale 400 -font \"Courier\" -pointsize 14 -fill \"#F00\" ");
        fprintf(fp, "-draw \'text 10,15 \"Exp(Z)    Z = {%le, %le}\"\' ", zR, zW);
        fprintf(fp, "-draw \'text 10,30 \"bottom-left = {%le, %le}\"\' ", xMin, yMin);
        fprintf(fp, "-draw \'text 10,45 \"top-right   = {%le, %le}\"\' ", xMax, yMax);
        fprintf(fp, " pic-wspaceexpfractal-%04d.gif &\n", runID);

        runID++;

    }


    while (runID < 900) {

        runIDd = (((double) runID) - 500.) / 400.;
        runIDd = sqrt( 1 - (runIDd - 1.) * (runIDd - 1.));

        xMin = runIDd * 0.382 + 0.36 * (1. - runIDd);
        xMax = runIDd * 0.388 + 0.45 * (1. - runIDd);
        yMin = runIDd * 0.018 + 0.01 * (1. - runIDd);
        yMax = runIDd * 0.024 + 0.10 * (1. - runIDd);

        fprintf(fp,
                "./calcWExpZ %d %le %le %le %le %le %le %d\n",
                runID,
                zR,
                zW,
                xMin,
                xMax,
                yMin,
                yMax,
                resX);

        fprintf(fp, "convert pic-wspaceexpfractal-%04d.xpm ", runID);
        fprintf(fp, "-scale 400 -font \"Courier\" -pointsize 14 -fill \"#F00\" ");
        fprintf(fp, "-draw \'text 10,15 \"Exp(Z)    Z = {%le, %le}\"\' ", zR, zW);
        fprintf(fp, "-draw \'text 10,30 \"bottom-left = {%le, %le}\"\' ", xMin, yMin);
        fprintf(fp, "-draw \'text 10,45 \"top-right   = {%le, %le}\"\' ", xMax, yMax);
        fprintf(fp, " pic-wspaceexpfractal-%04d.gif &\n", runID);

        runID++;

    }

    while (runID < 1300) {

        runIDd = (((double) runID) - 900.) / 400.;
        runIDd = sqrt( 1 - (runIDd - 1.) * (runIDd - 1.));

        xMin = runIDd * 0.38455 + 0.382 * (1. - runIDd);
        xMax = runIDd * 0.38465 + 0.388 * (1. - runIDd);
        yMin = runIDd * 0.02065 + 0.018 * (1. - runIDd);
        yMax = runIDd * 0.02075 + 0.024 * (1. - runIDd);

        fprintf(fp,
                "./calcWExpZ %d %le %le %le %le %le %le %d\n",
                runID,
                zR,
                zW,
                xMin,
                xMax,
                yMin,
                yMax,
                resX);

        fprintf(fp, "convert pic-wspaceexpfractal-%04d.xpm ", runID);
        fprintf(fp, "-scale 400 -font \"Courier\" -pointsize 14 -fill \"#F00\" ");
        fprintf(fp, "-draw \'text 10,15 \"Exp(Z)    Z = {%le, %le}\"\' ", zR, zW);
        fprintf(fp, "-draw \'text 10,30 \"bottom-left = {%le, %le}\"\' ", xMin, yMin);
        fprintf(fp, "-draw \'text 10,45 \"top-right   = {%le, %le}\"\' ", xMax, yMax);
        fprintf(fp, " pic-wspaceexpfractal-%04d.gif &\n", runID);

        runID++;

    }

    fclose(fp);

}
