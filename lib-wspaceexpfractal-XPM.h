// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// lib-wspaceexpfractal-XPM.h
// ----------------------------------------------------------------------
// Utility functions for writing a bitmap to an XPM3 file
// Example:
/*
    if (openLog(0) == 0) return;
    writeTimeToLog();
    initGlobalVariables();
       // ... (set variables here for current picture) ...
    if (calculateGlobalHelperVariables() == 0) { closeLog(); return; }
    dumpGlobalVariables();
       // ... (do calculation) ...
    writeBitmapToFile(0);
    closeLog();
*/
// ---------------------------------------------------------------------- 

int writeBitmapToFile(int runID) {
// Writes the entire bitmap contained in the
// in outBitmap[] array to a file. Bitmap will
// be either black or white, using XPM2 format.
//
// IN:   runID           identifier of the current run
//       outBitmap[]     holds the bitmap
//       resX            X resolution (pixel width)
//       resY            Y resolution (pixel height)
//       picFileString   string for output bitmap name
// OUT:  Bitmap file will be written;
//       returns -1 on success, otherwise 0 on failure

    char   bitmapFileName[200];    // plenty of space for the file name
    int    dummy;                  // throwaway
    int    curX;                   // pointer to current X position
    int    curY;                   // pointer to current Y position
    int    curPoint;               // pointer to current bitmap position

//  open the bitmap file
    dummy = sprintf(bitmapFileName, picFileString, runID);
    printf("- open file for output: "); printf(bitmapFileName); printf("\n");
    fpOutBitmap = fopen(bitmapFileName, "wb");

    if (fpOutBitmap == NULL) {
        printf("[ERROR] Bitmap file error; file name: '");
        printf(bitmapFileName);
        printf("'.\nPANIC - bitmap file didn't open\n\n");
        return 0;
    } 

//  print header in XPM3 format, two colors (black and white)
    fprintf(fpOutBitmap, "/* XPM */\n");
    fprintf(fpOutBitmap, "static char * XFACE[] = {\n");
    fprintf(fpOutBitmap, "\"%d %d 2 1\",\n", resX, resY);
    fprintf(fpOutBitmap, "\". c #FFFFFF\",\n");
    fprintf(fpOutBitmap, "\"@ c #000000\"\n");

//  print the picture
    curY = resY;
    curPoint = 0;

    while (curY-- > 0) {
    // one row

        fprintf(fpOutBitmap, ",\n\"");

        curX = resX;

        while (curX-- > 0) {
        // one pixel in that row

            if (outBitmap[curPoint] == 0) {
            //  white / no point

                fprintf(fpOutBitmap, ".");

            } else {
            //  black / point

                fprintf(fpOutBitmap, "@");

            }
           curPoint++;

        }
        fprintf(fpOutBitmap, "\"");

    }

    fprintf(fpOutBitmap, "}\n");

//  close file and exit
    fclose(fpOutBitmap);
    return -1;

}

// ---------------------------------------------------------------------- 

void initGlobalVariables() {
// resets all global variables to their defaults
// (i.e. prepare for a "clean slate")

    int curPoint;

    curPoint = maxBitmapPixels;
    while (curPoint-- > 0) outBitmap[curPoint] = 0;

    resX  =  50 ;
    zR    =   0.;
    zW    =   1.;
    xMin  =  -2.;
    xMax  =   2.;
    yMin  =  -2.;
    yMax  =   2.;
    DEBUG =   0 ;
    resizeFlag = false;

    curPoint = maxPyramidPoints;
    while (curPoint-- > 0) {
        YnEndpoint[curPoint] = 0;
    }

    curPoint = maxPyramidDepth;
    while (curPoint-- > 0) {
        YnLevelPtr[curPoint] = 0;
        YnLevelXMin[curPoint] = 0.;
        YnLevelXMax[curPoint] = 0.;
        YnLevelYMin[curPoint] = 0.;
        YnLevelYMax[curPoint] = 0.;
    }

}

// ---------------------------------------------------------------------- 

int calculateGlobalHelperVariables() {
// This routine first validates the required
// global variables, to be in the allowed
// parameter range; then, it calculates the
// dependent ("helper") variables that are
// global. Expects log file to be open (on validation error).

//  first part: validation
    if (xMin >= xMax) {
        dumpGlobalVariables();
        sprintf(logTxt, "[ERROR] Validation error: xMin must be smaller than xMax. Aborting.\n");
        writeLog();
        return 0;
    }

    if (yMin >= yMax) {
        dumpGlobalVariables();
        sprintf(logTxt, "[ERROR] Validation error: yMin must be smaller than yMax. Aborting.\n");
        writeLog();
        return 0;
    }

    if (resX < 10) {
        dumpGlobalVariables();
        sprintf(logTxt,
                "[ERROR] Validation error: X-resolution must be 10 or greater. Aborting.\n");
        writeLog();
        return 0;
    }

    if (resX > maxBitmapWidth) {
        dumpGlobalVariables();
        sprintf(logTxt,
                "[ERROR] Validation error: X-resolution must be %d or less. Aborting.\n",
                maxBitmapWidth);
        writeLog();
        return 0;
    }

//  second part: calculate dependent global ("helper") variables
    dPix = (xMax - xMin) / ((double) resX);
    resY = (int) (((double) resX) * (yMax - yMin) / (xMax - xMin));

//  validation of dependent variables
    if (resY < 10) {
        dumpGlobalVariables();
        sprintf(logTxt,
                "[ERROR] Error in dependent variable: Y-resolution must be 10 or greater. Aborting.\n");
        writeLog();
        return 0;
    }

    if (resY > maxBitmapHeight) {
        dumpGlobalVariables();
        sprintf(logTxt,
                "[ERROR] Error in dependent variable: Y-resolution must be %d or less. Aborting.\n",
                maxBitmapHeight);
        writeLog();
        return 0;
    }

    return -1;

}

// ---------------------------------------------------------------------- 

void dumpGlobalVariables() {
// prepares a log dump of global variables;
// expects log file to be open and ready to receive

    writeTimeToLog();

    sprintf(logTxt, "Dump global variables:\n"); writeLog();
    sprintf(logTxt, "- runID=%d\n", runID); writeLog();
    sprintf(logTxt, "- resX=%d\n", resX); writeLog();
    sprintf(logTxt, "- resY=%d\n", resY); writeLog();
    sprintf(logTxt, "- zR=%le\n", zR); writeLog();
    sprintf(logTxt, "- zW=%le\n", zW); writeLog();
    sprintf(logTxt, "- xMin=%le\n", xMin); writeLog();
    sprintf(logTxt, "- xMax=%le\n", xMax); writeLog();
    sprintf(logTxt, "- yMin=%le\n", yMin); writeLog();
    sprintf(logTxt, "- yMax=%le\n", yMax); writeLog();
    sprintf(logTxt, "- dPix=%le\n", dPix); writeLog();

    if (DEBUG) {
        sprintf(logTxt, "- DEBUG=%d (on)\n", DEBUG); writeLog();
    } else {
        sprintf(logTxt, "- DEBUG=%d (off)\n", DEBUG); writeLog();
    }

    if (resizeFlag) {
        sprintf(logTxt, "- auto-resize=%d (on)\n", resizeFlag); writeLog();
    } else {
        sprintf(logTxt, "- auto-resize=%d (off)\n", resizeFlag); writeLog();
    }

}

// ---------------------------------------------------------------------- 

void setPointOnBitmap(double valR, double valW) {
//  This method plots a point onto the bitmap.
//  IN:  valR, valW       the point to be plotted
//       outBitmap[]      the output bitmap
//       xMin, xMax       left / right bound of bitmap
//       yMin, yMax       lower / upper bound of bitmap
//       resX, resY       number of pixels (int)
//       dPix             pixel size

    int plotX;       // x coordinate on plot
    int plotY;       // y coordinate on plot

    if (valR < xMin) return;
    if (valR > xMax) return;
    if (valW < yMin) return;
    if (valW > yMax) return;

    plotX = (int) ((valR - xMin) / dPix);
    if (plotX < 0) plotX = 0;
    if (plotX >= resX) plotX = resX - 1;

    plotY = resY - ((int) ((valW - yMin) / dPix));
    if (plotY < 0) plotY = 0;
    if (plotY >= resY) plotY = resY - 1;

//  and set the point:
    outBitmap[plotX + resX * plotY] = 1;

}

// ---------------------------------------------------------------------- 
