// ===========================================================================
// W space - Exp Z fractal
// (CC) 2008 Jens Koeplinger
// http://www.jenskoeplinger.com/P
//
// Reference: J. A. Shuster and J. Koeplinger,
// "Elliptic complex numbers with dual multiplication."
// http://www.jenskoeplinger.com/P/PaperShusterKoepl_WSpace.pdf
//
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
//
// You are free:
// - to Share - to copy, distribute and transmit the work
// - to Remix - to adapt the work
//
// Under the following conditions:
// - Attribution. You must attribute the work in the manner specified by the
//   author or licensor (but not in any way that suggests that they endorse
//   you or your use of the work).
// - Share Alike. If you alter, transform, or build upon this work, you may
//   distribute the resulting work only under the same, similar or a compatible
//   license.
//
// For any reuse or distribution, you must make clear to others the license
// terms of this work. The best way to do this is with a link to:
//   http://creativecommons.org/licenses/by-sa/3.0
//
// Any of the above conditions can be waived if you get permission from the
// copyright holder.
//
// Nothing in this license impairs or restricts the author's moral rights.
// ===========================================================================

#define thisVersion       "W space - Exp Z fractal, version 0.9.5 (10 June 2008)"
#define licenseMain       "(CC) 2008 Jens Koeplinger - http://www.jenskoeplinger.com/P\n"
#define licenseDetail     "License: Creative Commons Attribution - Share Alike 3.0 Unported\n"
#define licenseReference  "http://creativecommons.org/licenses/by-sa/3.0\n"
#define logFileString     "log-wspaceexpfractal-%04d.txt"
#define logFileStringHelp "log-wspaceexpfractal-<run(dec)>.txt"
#define picFileString     "pic-wspaceexpfractal-%04d.xpm"
#define picFileStringHelp "pic-wspaceexpfractal-<run(dec)>.xpm"

#define maxLogString      10000
#define maxBitmapWidth    5000
#define maxBitmapHeight   5000
#define maxBitmapPixels   25000000
#define maxPyramidPoints  1000000
#define maxPyramidDepth   200

#define dPixCutoffFactor  1.

#define true -1
#define false 0

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// ---------------------------------------------------------------------------
// global declarations - functions
// ---------------------------------------------------------------------------

// lib-wspaceexpfractal-log.h
int    openLog(int);
void   writeTimeToLog();
void   writeLog();
void   closeLog();

// lib-wspaceexpfractal-XPM.h
int    writeBitmapToFile(int);
void   initGlobalVariables();
int    calculateGlobalHelperVariables();
void   dumpGlobalVariables();
void   setPointOnBitmap(double, double);

// lib-wspaceexpfractal-YnPyramid.h
void   calculateAndPruneYnPyramid();
int    calculateIterationDepth();
double calculateLevelEndpointMultiplier(int, int);

// lib-wspaceexpfractal-PrepareForPlot.h
void   prepareYnPyramidForPlotting();

// lib-wspaceexpfractal-Plot.h
void   plotExpZFractal();

// ---------------------------------------------------------------------------
// global declarations - variables
// ---------------------------------------------------------------------------

int    DEBUG;                        // 0 - no debugging; 1 - debugging on

FILE   *fpLog;                       // log file pointer
char   logTxt[maxLogString];         // throwaway string (for logging)
int    runID;                        // parameter to identify this run
                                     //   (will show in file names and log)

FILE   *fpOutBitmap;                 // output bitmap pointer
int    outBitmap[maxBitmapPixels];   // reserve maximum space
int    resX;                         // resolution in X (pixels width)
int    resY;                         // resolution in Y (pixels height)
double zR;                           // real part of current point Z (for Exp(Z))
double zW;                           // W axis part of current Z (for Exp(Z))
double xMin;                         // left bound of output bitmap
double xMax;                         // right boung of bitmap
double yMin;                         // lower bound
double yMax;                         // upper bound
double dPix;                         // width (and height) of a single pixel
int    resizeFlag;                   // if true, recalculates bounding box after first trial run

int    totalPyramidPoints;           // total number of points in the Yn Pyramid - 1
double YnR[maxPyramidPoints];        // Yn pyramid, real parts
double YnW[maxPyramidPoints];        // Yn pyramid, W parts
int    YnEndpoint[maxPyramidPoints]; // 0 if deeper levels to be computed

int    totalPyramidDepth;            // Yn pyramid, deepest level
int    YnLevelPtr[maxPyramidDepth];  // pointer to each level (by depth)

double YnLevelXMin[maxPyramidDepth]; // minimum X contribution from this level
double YnLevelXMax[maxPyramidDepth]; // maximum X contribution from this level
double YnLevelYMin[maxPyramidDepth]; // minimum Y contribution from this level
double YnLevelYMax[maxPyramidDepth]; // maximum Y contribution from this level

// ---------------------------------------------------------------------------
// main
// ---------------------------------------------------------------------------

int main(int argc, char **argv) {
//  here it starts: prepares the log; actual
//  execution is driven in "run-wspaceexpfractal.c"

    runID = 0; // default runID parameter is zero
    if (argc > 1) runID = atoi(argv[1]);

    if (openLog(runID) == 0) return;

    sprintf(logTxt, licenseMain);      writeLog();
    sprintf(logTxt, licenseDetail);    writeLog();
    sprintf(logTxt, licenseReference); writeLog();

    writeTimeToLog();
    sprintf(logTxt, "================Start.\n"); writeLog();

#include "run-wspaceexpfractal.c"

    writeTimeToLog();
    sprintf(logTxt, "All done.=============\n"); writeLog();

    closeLog();

    printf("Good-bye\n");

}

// ---------------------------------------------------------------------------
// include files (libraries)
// ---------------------------------------------------------------------------

#include "lib-wspaceexpfractal-log.h"
#include "lib-wspaceexpfractal-XPM.h"
#include "lib-wspaceexpfractal-YnPyramid.h"
#include "lib-wspaceexpfractal-PrepareForPlot.h"
#include "lib-wspaceexpfractal-Plot.h"
