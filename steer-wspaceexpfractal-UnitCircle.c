// ===========================================================================
// This is a quick-and-dirty steering program that procudes Linux shell
// scripts to steer the W Space Exp(Z) fractal computation.
//
// It uses ImageMagick for image manipulation:
/*

Convert from XPM to GIF using ImageMagick:
    convert <input file>.xpm <options> <output file>.gif

where <options> may include:
    -scale <width>                    Scale picture to <witdh> (keep height ratio)
    -font "<name>"                    selects print font, e.g. "Courier"
    -pointsize <size>                 size of characters in point
    -fill "#<rgb>"                    set draw color
    -draw 'text <x>,<y> "<string>"'   draws <string> at position <x>,<y> from top-left

*/
// You can then e.g. use gifsicle to make an animated GIF, e.g.:
/*

    gifsicle --delay=10 --colors 256 --loop *.gif > anim.gif

*/
// ===========================================================================

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

FILE   *fp;                          // out file pointer

int    runID;
double runIDd;
int    runIDstart;
double zR;
double zW;
double xMin;
double xMax;
double yMin;
double yMax;
int    resX;
int    steps;

// ---------------------------------------------------------------------------
// main
// ---------------------------------------------------------------------------

int main(int argc, char **argv) {
//  here it starts: prepares the log; actual
//  execution is driven in "run-wspaceexpfractal.c"

    fp = fopen("execSteerWExpZ_UnitCircle.sh", "wb");
    fprintf(fp, "# command file generated: \n");

    struct tm *local;
    time_t t;

    t = time(NULL);
    local = localtime(&t);
    fprintf(fp, "# %s\n", asctime(local));
    fprintf(fp, "date\n");

    xMin  =    0.  ;
    xMax  =    3.5 ;
    yMin  =   -2.5 ;
    yMax  =    2.5 ;
    resX  = 1000   ;
    runID = 2000   ;
    steps =  500   ;

    runIDstart = runID;

    while (runID < (runIDstart + steps)) {

        runIDd = (((double) runID) - ((double) runIDstart)) / ((double) steps);

        zR = cos(2 * 3.141592 * runIDd);
        zW = sin(2 * 3.141592 * runIDd);

        fprintf(fp,
                "./calcWExpZ %d %le %le %le %le %le %le %d\n",
                runID,
                zR,
                zW,
                xMin,
                xMax,
                yMin,
                yMax,
                resX);

        fprintf(fp, "convert pic-wspaceexpfractal-%04d.xpm ", runID);
        fprintf(fp, "-scale 400 -font \"Courier\" -pointsize 14 -fill \"#F00\" ");
        fprintf(fp, "-draw \'text 10,15 \"Exp(Z)    Z = {%le, %le}\"\' ", zR, zW);
        fprintf(fp, "-draw \'text 10,30 \"bottom-left = {%le, %le}\"\' ", xMin, yMin);
        fprintf(fp, "-draw \'text 10,45 \"top-right   = {%le, %le}\"\' ", xMax, yMax);
        fprintf(fp, " pic-wspaceexpfractal-%04d.gif\n", runID);

        runID++;

    }

    fclose(fp);

}
