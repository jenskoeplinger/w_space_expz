// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// run-wspaceexpfractal.c
// ----------------------------------------------------------------------
// This code below is included into main() and drives the calculation.
// It reads the command line arguments, parses them, and then kicks
// off the fractal calculation. See wspaceexpfractal.c for place of
// inclusion.
// ---------------------------------------------------------------------- 

    initGlobalVariables();

//  process command line arguments
//  first, dump all arguments

    int i;

    if ((argc == 1) || (argv[1][0] == '-')) {
    //  no command line argument given,
    // or first argument started with dash;
    // --> print help and abort

#include "help-wspaceexpfractal.c"

        closeLog();
        return;

    } else {
    //  at least one command line argument given; display raw

        sprintf(logTxt, "[NOTE] For help: run without arguments\n"); writeLog();
        sprintf(logTxt, "-------------------------------------\n"); writeLog();
        sprintf(logTxt, "Number of command line arguments = %d\n", argc); writeLog();

        for (i = 0; i < argc; i++) {
            sprintf(logTxt, "- Arg. #%d = \"%s\"\n", i, argv[i]); writeLog();
        }

    }

// ---------------------------------------------------------------------- 
//  second, (naively) parse command line parameters, if present, and
//  convert them, and overwrite global variables

    if (argc > 2) zR = atof(argv[2]);
    if (argc > 3) zW = atof(argv[3]);
    if (argc > 4) xMin = atof(argv[4]);
    if (argc > 5) xMax = atof(argv[5]);
    if (argc > 6) yMin = atof(argv[6]);
    if (argc > 7) yMax = atof(argv[7]);
    if (argc > 8) resX = atoi(argv[8]);
    if (argc > 9) DEBUG = atoi(argv[9]);
    if (argc > 10) resizeFlag = atoi(argv[10]);

    if (argc > 11) {
    //  ignore any additional arguments, but print a warning

        sprintf(logTxt,
                "[INFO] More than 10 arguments provided; ignoring the additional ones.\n");
        writeLog();

    }

// ---------------------------------------------------------------------- 
//  third, do some basic validation, and calculate
//  dependent ("helper") variables

    if (calculateGlobalHelperVariables() == 0) { closeLog(); return; }

    dumpGlobalVariables();

// ---------------------------------------------------------------------- 
//  all ready: do the actual calculation!

    calculateAndPruneYnPyramid();
    prepareYnPyramidForPlotting();

    if (resizeFlag) {
    //  auto-resize is on; take the calculated min/max bounds of
    //  the x and y axis, and recalculate the bounding box to fit
    //  the entire picture into the bitmap; keep the initial aspect
    //  ratio resY/resX

        sprintf(logTxt, "Auto-resize selected; recalculate bounding box parameters.\n");
        writeLog();

        xMin = YnLevelXMin[0] - 2. * dPix;
        xMax = YnLevelXMax[0] + 2. * dPix;
        yMin = YnLevelYMin[0] - 2. * dPix;
        yMax = YnLevelYMax[0] + 2. * dPix;

    //  this is the new parameter space; now fit into the same-size bitmap
    //  as initially requested by the user

        double xyRatio;     // target ratio of bitmap
        double paramRatio;  // fractal parameter ratio
        double newMin;      // throwaway new minimum value
        int curPoint;       // throwaway pointer

        xyRatio = ((double) resY) / ((double) resX);
        paramRatio = (yMax - yMin) / (xMax - xMin);

        if (xyRatio > paramRatio) {
        //  the target ratio requires a higher picture

            newMin = yMin - (xyRatio / paramRatio - 1.) * (yMax - yMin) / 2.;
            yMax   = yMax + (xyRatio / paramRatio - 1.) * (yMax - yMin) / 2.;
            yMin   = newMin;

        } else {
        //  the target ratio requires a wider picture

            newMin = xMin - (paramRatio / xyRatio - 1.) * (xMax - xMin) / 2.;
            xMax   = xMax + (paramRatio / xyRatio - 1.) * (xMax - xMin) / 2.;
            xMin   = newMin;

        }

    //  new parameters calculated; re-run with the modifications
        totalPyramidPoints = 0;
        totalPyramidDepth = 0;

        curPoint = maxPyramidPoints;

        while (curPoint-- > 0) {
            YnR[curPoint] = 0.;
            YnW[curPoint] = 0.;
            YnEndpoint[curPoint] = 0;
        }

        if (calculateGlobalHelperVariables() == 0) { closeLog(); return; }
        dumpGlobalVariables();
        calculateAndPruneYnPyramid();
        prepareYnPyramidForPlotting();

    }

    plotExpZFractal();

// ---------------------------------------------------------------------- 
//  calculation done; write the bitmap to file

    writeTimeToLog();
    sprintf(logTxt, "Write bitmap to file.\n"); writeLog();

    writeBitmapToFile(runID);
