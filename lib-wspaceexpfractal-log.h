// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// lib-wspaceexpfractal-log.h
// ----------------------------------------------------------------------
// Contains methods for logging (screen and file).
// Example:
/*
    if (openLog(0) == 0) return;
    writeTimeToLog();
    sprintf(logTxt, "This is a test.\n"); writeLog();
    closeLog();
*/
// ---------------------------------------------------------------------- 

int openLog(int runID) {
//  open log file in the current directory, with
//  file name described by the global constant "logFileString",
//  which contains a %d for the "runID" parameter (optional)
//
//  IN:  runID    arbitrary integer (to be added to file name)
//  OUT: log file opened, with message printed to screen and file;
//       returns -1 on success, and 0 on failure

    char   logFileName[200];       // plenty of space for the file name
    int    dummy;                  // throwaway

    dummy = sprintf(logFileName, logFileString, runID);
    fpLog = fopen(logFileName, "wb");

    if (fpLog == NULL) {
        printf("Log file error; file name: '");
        printf(logFileName);
        printf("'.\nPANIC - logfile didn't open\n\n");
        return 0;
    }

    printf("Logfile opened: '");
    printf(logFileName);
    printf("'.\n");

    fprintf(fpLog, thisVersion);
    fprintf(fpLog, "\n");

    printf(thisVersion);
    printf("\n");

    return -1;

}

// ---------------------------------------------------------------------- 

void writeTimeToLog() {
// Prints the current time to the log, preceded
// by a "> " sequence for parsing
//
// IN: fpIn is a pointer to an open file (for writing)

    struct tm *local;
    time_t t;

    t = time(NULL);
    local = localtime(&t);
    printf("[time] %s", asctime(local));
    fprintf(fpLog, "[time] %s", asctime(local));

}

// ---------------------------------------------------------------------- 

void writeLog() {
// Prints the contents of the global logTxt[] array
// to the standard output, and to an open log file.
//
// IN: fpIn is a pointer to an open file (for writing)
//     logTxt[] array holds the string to be logged

    printf(logTxt);
    fprintf(fpLog, logTxt);

}

// ---------------------------------------------------------------------- 

void closeLog() {
// Closes the log file

    fclose(fpLog);

}
