// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// lib-wspaceexpfractal-PrepareForPlot.h
// ----------------------------------------------------------------------
// This library takes the existing Y(n) pyramid and prepares
// it for plotting: For every level, the minimum and maximum
// total X- and Y-contribution will be calculated (i.e. the
// min and max values of which any combination of additions
// from this level on down could ever affect the plot). These
// values will later be used to abort calculations for points
// that are outside the plot boundaries, and will never be able
// to contribute to the bitmap file anymore. For graphs that
// have a significant number of points outside the plot area
// (e.g. large arguments) this is an essential method of
// pruning.
//
// Example of use:
/*
    prepareYnPyramidForPlotting();
*/
// ---------------------------------------------------------------------- 

void prepareYnPyramidForPlotting() {
//  Begin at deepest level in the pyramid, and loop through all
//  points in that level. Calculate the minimum and maximum
//  possible contribution, from any point in that level, for
//  horizontal and vertical components separately.
//
//  Then, loop through all points of the next highest level,
//  (totalPyramidDepth - 1), and do the same.
//
//  For all levels, record the minimum and maximum contributions
//  into the YnLevel{X|Y}{Min|Max} arrays, to contain not just
//  the min/max contributions from the respective level, but also
//  added up from all deeper levels. This will allow at the end
//  to make a quick determination of whether a pixel can still
//  fall into the plottable area, or whether that pixel will be
//  outside and calculation can be aborted.
//
//  IN:  totalPyramidDepth       number (and pointer) of deepest level
//       totalPyramidPoints      pointer to the hightest point index
//       YnR[point], YnW[point]  real and W parts of each point
//       YnLevelPtr[level]       pointer to first point in each level
//
//  OUT: YnLevel{X|Y}{Min|Max}[level]

    int    curLevel;             // current level being processed
    int    curPoint;             // pointer to the current point
    int    pointCtr;             // throwaway counter for points processed
    double curXMin;              // throwaway for determining min X of level
    double curXMax;              // ... max X
    double curYMin;              // ... min Y
    double curYMax;              // ... max Y

    sprintf(logTxt, "Prepare Y(n) pyramid for plotting: Calculate "); writeLog();
    sprintf(logTxt, "minimum and maximum X- and Y- axis contributions.\n"); writeLog();

    curLevel = totalPyramidDepth;

    while (curLevel >= 0) {
    //  process each level for min- and max- contribution
    //  in x and y direction. Then, combine with lower-level
    //  contributions (if any).

        curXMin =  1.e50;
        curXMax = -1.e50;
        curYMin =  1.e50;
        curYMax = -1.e50;
        pointCtr = 0;

        curPoint = YnLevelPtr[curLevel];

        while (curPoint <= totalPyramidPoints) {
        //  loop through all points in the level; compare;
        //  once last point has been processed, set
        //  curPoint to totalPyramidPoints + 1

            if (YnR[curPoint] < curXMin) curXMin = YnR[curPoint];
            if (YnR[curPoint] > curXMax) curXMax = YnR[curPoint];
            if (YnW[curPoint] < curYMin) curYMin = YnW[curPoint];
            if (YnW[curPoint] > curYMax) curYMax = YnW[curPoint];

            curPoint++;
            pointCtr++;

            if (curLevel == totalPyramidDepth) {
            //  this is the deepest level; exit condition
            //  is to reach the last point; no need
            //  to do anothing (because the loop already
            //  checks on curPoint <= totalPyramidPoints)

            } else {
            //  check whether we've reached the next deepest level

                if (curPoint >= YnLevelPtr[curLevel + 1]) {
                //  the pointer has been moved to the next deeper level
                //  => abort (exit condition reached)

                    curPoint = totalPyramidPoints + 1;

                }

            }

        }

    //  current level finished min/max calculation for level; log
        sprintf(logTxt, "[level %d] Points: %d\n", curLevel, pointCtr); writeLog();
        sprintf(logTxt, "- [current]   minX: %le, maxX: %le, ", curXMin, curXMax); writeLog();
        sprintf(logTxt, "minY: %le, maxY: %le\n", curYMin, curYMax);  writeLog();

    //  aggregate with deeper level (if any)
        if (curLevel < totalPyramidDepth) {
        //  add deeper contributions also

            curXMin = curXMin + YnLevelXMin[curLevel + 1];
            curXMax = curXMax + YnLevelXMax[curLevel + 1];
            curYMin = curYMin + YnLevelYMin[curLevel + 1];
            curYMax = curYMax + YnLevelYMax[curLevel + 1];

        }

    //  store values for current level, and log
        YnLevelXMin[curLevel] = curXMin;
        YnLevelXMax[curLevel] = curXMax;
        YnLevelYMin[curLevel] = curYMin;
        YnLevelYMax[curLevel] = curYMax;

        sprintf(logTxt, "- [aggregate] minX: %le, maxX: %le, ", curXMin, curXMax); writeLog();
        sprintf(logTxt, "minY: %le, maxY: %le\n", curYMin, curYMax);  writeLog();

    //  and advance to the next level
        curLevel--;

    }

//  all done; exit
    sprintf(logTxt, "Y(n) pyramid is now prepared for plotting.\n"); writeLog();

}
