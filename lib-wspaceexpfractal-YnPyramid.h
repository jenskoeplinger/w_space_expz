// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// lib-wspaceexpfractal-YnPyramid.h
// ----------------------------------------------------------------------
// This library builds the initial Y(n) pyramid. Upon completion
// of each level, it also prunes the pyramid by determining
// "endpoints", where further (deeper) levels derived from that
// point do not contribute to changes in the graph anymore. The
// log file is expected to be open.
//
// Example:
/*
    calculateAndPruneYnPyramid();
*/
// ---------------------------------------------------------------------- 

void calculateAndPruneYnPyramid() {
//  main entry point; seed Y(0) and Y(1); then, for each
//  deeper level, loop through all members in the previous
//  level, and if they're not marked as end point, calculate
//  the two new members of the current level. Once the level
//  is complete, loop through all members in the level and
//  mark endpoints.
//
//  The resulting pruned pyramid will have all values of
//  level N divided by N!, i.e. it'll contain Y(N)/N!
//  entries.
//
//  When called, all arrays and variables are expected to
//  be reset.

    int currentPyramidPoint;  // pointer to the first entry of current level
    int prevPyramidPoint;     // pointer to a point in the next-higher level
    int prevPyramidPointMax;  // highest point to point in next-higher level
    int currentPyramidDepth;  // current level
    double currentPyramidDepthD; // (same, as double; throwaway)
    int levelPointCount;      // counter for points in the current level
    int nonEndpointCount;     // counter for non-endpoints in current level

    double curR;              // throwaway to current point (real)
    double curW;              // throwaway to current point (W)
    double newR;
    double newW;
    int thisMaxPyramidDepth;  // deepest iteration level for this iteration
    double currentLevelEndpointMultiplier;
                              // if the max-norm of the current pyramid point,
                              // multiplied with currentLevelEndpointMultiplier,
                              // is smaller than 0.5, then mark this as an
                              // endpoint.
    double testEndpoint;

    int exitCondition;        // throwaway; if 0 then continue loop; otherwise exit

    sprintf(logTxt, "Calculate and prune Y(n) Pyramid.\n"); writeLog();

//  calculate helper variables
    thisMaxPyramidDepth = calculateIterationDepth();
    sprintf(logTxt, "Estimated maximum iteration depth: %d.\n", thisMaxPyramidDepth); writeLog();

    if (thisMaxPyramidDepth >= maxPyramidDepth) {
    //  in order to calculate the desired precision, the pyramid depth
    //  required would be in excess of the maximum allowed

        sprintf(logTxt,
                "[NOTE] This calculation will be truncated at depth: %d.\n",
                maxPyramidDepth);
        writeLog();

    }

//  seed Y(0)
    YnR[0] = 1.;
    YnW[0] = 0.;
    YnEndpoint[0] = 0;
    YnLevelPtr[0] = 0;

//  seed Y(1)
    YnR[1] = zR;
    YnW[1] = zW;
    YnEndpoint[1] = 0;
    YnLevelPtr[1] = 1;

//  prepare variables for concurrent level calculation
    currentPyramidPoint = 2; // set pointer to next available free pointer
    prevPyramidPoint = 1; // set to ("first") point on the previous level
    prevPyramidPointMax = 1; // the first point is also the only one
    currentPyramidDepth = 2; // we're calculating level 2 now
    exitCondition = 0;

    sprintf(logTxt, "Begin at level (depth) 2.\n"); writeLog();

    while (exitCondition == 0) {
    //  loop until an exitCondition is reached (see end of while loop)

        nonEndpointCount = 0;
        levelPointCount = 0;
        YnLevelPtr[currentPyramidDepth] = currentPyramidPoint;

        currentPyramidDepthD = (double) currentPyramidDepth;
        currentLevelEndpointMultiplier =
            calculateLevelEndpointMultiplier(thisMaxPyramidDepth, currentPyramidDepth);

        sprintf(logTxt,
                "[Level %d] endpoint multiplier: %le\n",
                currentPyramidDepth,
                currentLevelEndpointMultiplier);
        writeLog();

        while (prevPyramidPoint <= prevPyramidPointMax) {
        //  process one point from the previous (next-higher) level

            if (DEBUG) { sprintf(logTxt, "  [DEBUG 1]\n"); writeLog(); }

            if (YnEndpoint[prevPyramidPoint] == 0) {
            //  point in previous level was marked as non-endpoint
            //  --> multiply with Z, using both multiplications

                if (DEBUG) { sprintf(logTxt, "  [DEBUG 2]\n"); writeLog(); }

                curR = YnR[prevPyramidPoint];
                curW = YnW[prevPyramidPoint];

            //  multiply using X multiplication (i.e. W+ space)
                newR = ((curR * zR) - (curW * zW))               / currentPyramidDepthD;
                newW = ((curR * zW) + (curW * zR) + (curW * zW)) / currentPyramidDepthD;

                YnR[currentPyramidPoint] = newR;
                YnW[currentPyramidPoint] = newW;

                if (DEBUG) {
                    sprintf(logTxt,
                            "  - [DEBUG 2a] level %d new point (%le, %le)",
                            currentPyramidDepth,
                            newR,
                            newW);
                    writeLog();
                }

            //  probe for endpoint condition
                testEndpoint = sqrt(newR * newR + abs(newR * newW) + newW * newW); // max norm
                testEndpoint = testEndpoint * currentLevelEndpointMultiplier;

                if (testEndpoint < dPixCutoffFactor) {
                //  this is an endpoint; flag as such

                    YnEndpoint[currentPyramidPoint] = -1;
                    if (DEBUG) { sprintf(logTxt, " [endpoint]"); writeLog(); }

                } else {
                //  this is not an endpoint; update counter

                    nonEndpointCount++;

                }

                if (DEBUG) { sprintf(logTxt, "\n"); writeLog(); }

            //  first multiplication OK, advance to next pointer for
            //  O multiplication (W- space)
                levelPointCount++;
                currentPyramidPoint++;

                if (currentPyramidPoint < maxPyramidPoints) {
                //  pointer not (yet) out of range; process
                //  O multiplication (W- space)

                    if (DEBUG) { sprintf(logTxt, "  [DEBUG 3]\n"); writeLog(); }

                    newW = ((curR * zW) + (curW * zR) - (curW * zW)) / currentPyramidDepthD;

                    YnR[currentPyramidPoint] = newR;
                    YnW[currentPyramidPoint] = newW;

                    if (DEBUG) {
                        sprintf(logTxt,
                                "  - [DEBUG 3a] level %d new point (%le, %le)",
                                currentPyramidDepth,
                                newR,
                                newW);
                        writeLog();
                    }

                //  probe for endpoint condition
                    testEndpoint = sqrt(newR * newR + abs(newR * newW) + newW * newW); // max norm
                    testEndpoint = testEndpoint * currentLevelEndpointMultiplier;

                    if (testEndpoint < dPixCutoffFactor) {
                    //  this is an endpoint; flag as such

                        YnEndpoint[currentPyramidPoint] = -1;
                        if (DEBUG) { sprintf(logTxt, " [endpoint]"); writeLog(); }

                    } else {
                    //  this is not an endpoint; update counter

                        nonEndpointCount++;

                    }

                    if (DEBUG) { sprintf(logTxt, "\n"); writeLog(); }

                //  second point calculated OK; advance pointers
                    levelPointCount++;
                    currentPyramidPoint++;

                } // end if (pointer not out-of-range)

            } // end if (previous-level point was endpoint)

        //  previous-level point processed OK
        //  - advance previous-level pointer
            prevPyramidPoint++;

        //  check for exit condition: currentPyramidPoint >= maxPyramidPoints
            if (currentPyramidPoint >= maxPyramidPoints)
                prevPyramidPoint = prevPyramidPointMax + 1;

        } // end while (previous-level points)

    //  log progress
        sprintf(logTxt,
                "[completed %d] points: %d; non-endpoint: %d; total count: %d ",
                currentPyramidDepth,
                levelPointCount,
                nonEndpointCount,
                currentPyramidPoint);
        writeLog();
        writeTimeToLog();

    //  all points from the previous level have been processed
    //  - reset prevPyramidPoint and prevPyramidPointMax to new values
    //  - increase level (currentPyramidDepth; exit condition if > maxPyramidDepth)
        prevPyramidPoint = YnLevelPtr[currentPyramidDepth];
        prevPyramidPointMax = currentPyramidPoint - 1;
        currentPyramidDepth++;

    //  check for exitCondition:
        if (currentPyramidDepth >= maxPyramidDepth)   exitCondition = 1; // max allowable depth reached
        if (nonEndpointCount == 0)                    exitCondition = 2; // all points endpoints; end
        if (currentPyramidPoint >= maxPyramidPoints)  exitCondition = 3; // max pyramid points reached
        if (levelPointCount == 0)                     exitCondition = 4; // error (no point processed?)

    } // end while (levels / exitCondition == 0 )

//  pyramid calculation is finished;
//  do post-processing (variables and output)

    sprintf(logTxt, "Y(n) pyramid calculation and pruning finished.\n"); writeLog();

    totalPyramidDepth = currentPyramidDepth - 1;
    totalPyramidPoints = currentPyramidPoint - 1;

    sprintf(logTxt, "- totalPyramidDepth = %d\n", totalPyramidDepth); writeLog();
    sprintf(logTxt,
            "- totalPyramidPoints = %d (array pointer; add +1 for actual number of points)\n",
            totalPyramidPoints);
    writeLog();

    if (exitCondition == 1) {
    //  aborted due to pyramid depth limitation

        sprintf(logTxt, "[NOTE] Calculation needed to abort due to depth limitation.\n");
        writeLog();
        sprintf(logTxt, "[NOTE] Limiting constant: maxPyramidDepth = %d\n", maxPyramidDepth);
        writeLog();

    } else if (exitCondition == 2) {
    //  regular end: All points are endpoints.

        sprintf(logTxt, "[NOTE] Calculation completed normally.\n"); writeLog();

    } else if (exitCondition == 3) {
    //  aborted due to pyramid points limitation

        sprintf(logTxt, "[NOTE] Calculation needed to abort due to total point limitation.\n");
        writeLog();
        sprintf(logTxt, "[NOTE] Limiting constant: maxPyramidPoints = %d\n", maxPyramidPoints);
        writeLog();

    } else if (exitCondition == 4) {
    //  no points in pyramid

        sprintf(logTxt, "[ERROR] Pyramid does not contain any points.\n"); writeLog();

    } else {
    //  unknown exit condition; abort

        sprintf(logTxt, "[ERROR] Unknown exit condition: %d\n", exitCondition); writeLog();

    }

}

// ---------------------------------------------------------------------- 

int calculateIterationDepth() {
// This function calculates the estimated maximum iteration
// depth (level) at which calculation of the pyramid can be
// aborted, because all further (deeper) points can
// be pruned.
//
// IN:  zR, zW - the argument of Exp(Z)
//      dPix   - the width of a pixel
// OUT: the deepest estimated level that needs calculating
//      (integer; rounded up)
//
// Formula:
//
// N = 2|Z| + log2 (|Z| / dPix)
//
// where |Z| is the max norm sqrt(zR^2 + |zR zW| + zW^2)

    double absZMax;  // max norm of Z
    double nMaxD;    // return value as double

    absZMax = sqrt(zR * zR + abs(zR * zW) + zW * zW);
    nMaxD = 2. * absZMax + log(absZMax / dPix) / log(2.);

    if (nMaxD < 1.) nMaxD = 1.; // make at least 1 or bigger

    return (int) (nMaxD + 0.95);

}

// ---------------------------------------------------------------------- 

double calculateLevelEndpointMultiplier(int thisMaxPyramidDepth, int currentPyramidDepth) {
// This function calculates a multiplier, to be
// applied to the max norm of the current value
// in the pyramid. If the product is smaller than
// 0.5, then the pyramid calculation can be aborted.
//
// IN:  thisMaxPyramidDepth (currently not used)
//      currentPyramidDepth (n)
//      zR, zW
//      dPix
// OUT: multiplier (A)
//
// Formula:
//
// A := ( ( |Z|^(N - n) ) * (n - 1)! ) / ( (N - 1)! * dPix)
//
// where |Z| is the max norm sqrt(zR^2 + |zR zW| + zW^2),
// N is 2*|Z| and n is the current depth (when smaller than N).
// Returns |Z|/(n * dPix) when n >= N.

    double absZMax;     // max norm of Z
    int    absZMax2i;   // (int) absZMax * 2
    int    i;           // throwaway; current counter from N-n to n
    double retVal;      // return value; A

    absZMax = sqrt(zR * zR + abs(zR * zW) + zW * zW);
    absZMax2i = ((int) (absZMax + 0.9)) * 2;

    retVal = 1. / dPix;

    if (currentPyramidDepth >= absZMax2i) {
    //  reached point where Taylor series is smaller than 0.5^n
    //  => just return |Z| / (n * dPix)

       retVal = retVal * absZMax / ((double) currentPyramidDepth);

    } else {
    //  Tayor series still has parts larger than 0.5^n; check
    //  for potentially higher contributions at deeper levels

        i = currentPyramidDepth; // i = n

        while (i < absZMax2i) {
        //  loop 'i' from n to (N - 1)

            retVal = retVal * absZMax; // multiply with |Z|
            retVal = retVal / ((double) i); // divide by i

            i++;

        }

    }

//  that's it; return
    return retVal;

}
