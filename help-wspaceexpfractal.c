// ======================================================================
// W space - Exp Z fractal (CC) 2008 Jens Koeplinger
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "wspaceexpfractal.c" for what this means to you!
// ======================================================================
// help-wspaceexpfractal.c
// ----------------------------------------------------------------------
// Prints out help information. This section is included in
// run-wspaceexpfractal.c, and separated out for readability.
// ---------------------------------------------------------------------- 

                sprintf(logTxt, "\n----------INFO----------\n");
    writeLog(); sprintf(logTxt, "\nThis program calculates the Exp(Z) solution set in W space.\n\n");
    writeLog(); sprintf(logTxt, "Reference: J. A. Shuster and J. Koeplinger,\n");
    writeLog(); sprintf(logTxt, "\"Elliptic complex numbers with dual multiplication\".\n");
    writeLog(); sprintf(logTxt, "http://www.jenskoeplinger.com/P/PaperShusterKoepl_WSpace.pdf\n\n");
    writeLog(); sprintf(logTxt, "Command line parameters (in the following order):\n");
    writeLog(); sprintf(logTxt, "  <run>    - (required) arbitrary integer, to index the current run\n");
    writeLog(); sprintf(logTxt, "  [zR]     - real part of Z (default: %lf)\n", zR);
    writeLog(); sprintf(logTxt, "  [zW]     - real part of Z (default: %lf)\n", zW);
    writeLog(); sprintf(logTxt, "  [xMin]   - left bound of output bitmap (default: %lf)\n", xMin);
    writeLog(); sprintf(logTxt, "  [xMax]   - right bound of output bitmap (default: %lf)\n", xMax);
    writeLog(); sprintf(logTxt, "  [yMin]   - lower bound of output bitmap (default: %lf)\n", yMin);
    writeLog(); sprintf(logTxt, "  [yMax]   - upper bound of output bitmap (default: %lf)\n", yMax);
    writeLog(); sprintf(logTxt, "  [resX]   - number of pixels in X direction (default: %d)\n", resX);
    writeLog(); sprintf(logTxt, "  [debug]  - set to 1 to turn on debugging output\n");
    writeLog(); sprintf(logTxt, "             (default: 0 / off)\n");
    writeLog(); sprintf(logTxt, "  [resize] - set to 1 to resize the parameter bounds automatically\n");
    writeLog(); sprintf(logTxt, "             to fit the entire fractal (default: 0 / off)\n");
    writeLog(); sprintf(logTxt,
                        "\nOutput: Log file '%s' and XPM3 bitmap file '%s', ",
                        logFileStringHelp,
                        picFileStringHelp);
    writeLog(); sprintf(logTxt, "containing <run> in filename as identifier.\n");
    writeLog(); sprintf(logTxt, "\nExample:\n");
    writeLog(); sprintf(logTxt,
                        "  %s 0 0 1 -2 2 -2.2 2.2 100\n\n",
                        argv[0]);
    writeLog(); sprintf(logTxt, "In this example, the output files will contain the <run> index 0 ");
    writeLog(); sprintf(logTxt, "in the output file names, will calculate Exp({0, 1}), ");
    writeLog(); sprintf(logTxt, "and render a bitmap in the {[-2, 2], [-2.2, 2.2]} ");
    writeLog(); sprintf(logTxt, "interval at a width of 100 pixels.\n\n");
    writeLog();
